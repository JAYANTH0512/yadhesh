import java.util.HashMap;
import java.util.Set;

public class Map {
	public static void main(String[] args) {
		
		java.util.Map<String ,String> map = new HashMap<>();
		map.put("Name", "Jai");
		map.put("Actor", "Vijay");
		map.put("Actress", "Pallavi");
		
		Set<String> keys = map.keySet(); 
		for(String key : keys ) {
			
			System.out.println( key + "  " +map.get(key));
		}
	}

}
